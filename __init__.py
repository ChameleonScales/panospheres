# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published by
#    the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Panospheres",
	"author": "ChameleonScales (chameleonscales@protonmail.com)",
	"version": (1, 1, 0),
	"blender": (4, 0, 2),
	"location": "View3D > Sidebar > Panospheres Tab",
	"description": "import and manipulate panoramas for landscape simulations",
	"doc_url": "https://gitlab.com/ChameleonScales/panospheres/-/blob/master/README.md",
	"tracker_url": "https://gitlab.com/ChameleonScales/panospheres/-/issues",
	"warning": "",
	"category": "3D View"}

import bpy
from bpy.props import (StringProperty,
                       BoolProperty,
                       FloatProperty,
                       IntProperty,
                       EnumProperty,
                       PointerProperty,
                       )
from bl_ui.space_toolsystem_common import ToolSelectPanelHelper
import os
from math import (pi, radians, degrees)

def prefs():
	return bpy.context.preferences.addons[__name__].preferences

def update_Addon_category(self, context):
	from bpy.utils import unregister_class
	utils = bpy.utils

	imp = IMPORT_PT_ui
	utils.unregister_class(imp)
	imp.bl_category = self.category
	utils.register_class(imp)

	man = MANIPULATE_PT_ui
	utils.unregister_class(man)
	man.bl_category = self.category
	utils.register_class(man)

def ShowMessageBox(title = "Message Box", icon = 'INFO', text='message content'):
    def draw(self, context):
        self.layout.label(text=text)
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

def import_panosphere(self, context, PanoName, ImgPath, PanoHFOV, PanoRot, PanoLoc, PanoAltitude, PanoHeight, SunElev, SunRot):
		PS = context.scene.PS_props
		bpy.ops.image.open(filepath = ImgPath)

		# Take file name out of url
		ImgFilename = ImgPath.split("/",-1)[-1]

		# path to assets blend file containing template objects:
		templatePath = os.path.dirname(os.path.abspath(__file__))+"/assets.blend"
		# append objects:
		with bpy.data.libraries.load(templatePath, link=False) as (data_from, data_to):
			data_to.objects = data_from.objects
			# append equi or cyl Geometry Nodes group
			if PS.projection == "equi":
				data_to.materials = ["Panorama equi"]
				if not bpy.data.node_groups.get('Panosphere equi'):
					data_to.node_groups.append("Panosphere equi")
			elif PS.projection == "cyl":
				data_to.materials = ["Panorama cyl"]
				if not bpy.data.node_groups.get('Panosphere cyl'):
					data_to.node_groups.append("Panosphere cyl")

		# Rename and set values of appended libraries
		mat = data_to.materials[0]
		if PS.projection == "equi":
			mat.name = 'Panorama equi ' + PanoName
			mat.node_tree.nodes[1].node_tree.name = 'Pano equi ' + PanoName
		elif PS.projection == "cyl":
			mat.name = 'Panorama cyl ' + PanoName
			mat.node_tree.nodes[1].node_tree.name = 'Pano cyl ' + PanoName
		for obj in data_to.objects:
			if obj.name.startswith('Panosphere'):
				obj.name = PanoName + '_Pano'
				obj.data.name = PanoName + '_Pano'
				obj.data.use_fake_user=True
				PanoSphere = obj
				PanoSphere.rotation_euler[2]=PanoRot
			if obj.name.startswith('Camera-render'):
				obj.name = PanoName + '_Camera-render'
				obj.data.name = PanoName + '_Camera'
				CamRender = obj
				CamRender.rotation_euler[2]=PanoRot
			if obj.name.startswith('Camera-navigation'):
				obj.name = PanoName + '_Camera-navigation'
				CamNavi = obj
				CamNavi.rotation_euler[2]=PanoRot

		# apply equi or cyl Geometry Nodes and material to panosphere object
		modifier = PanoSphere.modifiers["Panosphere"]
		matSlot = PanoSphere.material_slots[0]
		if PS.projection == "equi":
			modifier.node_group = bpy.data.node_groups['Panosphere equi']
		elif PS.projection == "cyl":
			modifier.node_group = bpy.data.node_groups['Panosphere cyl']
		matSlot.material = bpy.data.materials[mat.name]

		# set material parameters:
		MatNodes = matSlot.material.node_tree.nodes[1].node_tree
		img = bpy.data.images[ImgFilename]
		matImg = MatNodes.nodes['Image Texture']
		matImg.image = img

		if PS.img_color_space != "default":
			matImg.image.colorspace_settings.name = PS.img_color_space

		# set Geometry Nodes parameters:
		mat_socket_id = modifier.node_group.interface.items_tree['Material'].identifier
		hfov_socket_id = modifier.node_group.interface.items_tree['HFOV'].identifier
		modifier[mat_socket_id] = mat
		modifier[hfov_socket_id] = float(PanoHFOV)
		
		if PS.create_scene == True:
			'''
			##### WARNING : I USE THE OPERATOR WHICH IS NOT A GREAT PRACTICE. DOING IT THE FULL 'DATA' WAY MIGHT GIVE BETTER PERFORMANCE ON LARGE SCENES WITH MANY COPIES TO CREATE :
			'''
			bpy.ops.scene.new(type='LINK_COPY')
			bpy.context.scene.name = PanoName

		if PS.create_ground_parent == False:
			top_parent = CamRender
			altitude_parent = CamRender
		else:
			# add ground empty:
			Foot = bpy.data.objects.new(name = PanoName + '_Foot', object_data = None)
			Foot.lock_rotation[0:3] = (True, True, True)
			if PS.create_point_cols == False : # link to the current collection:
				bpy.context.collection.objects.link(Foot)
			CamRender.location[2] = PanoHeight
			CamRender.parent = Foot
			CamNavi.location[2] = PanoHeight
			CamNavi.parent = Foot
			altitude_parent = Foot
			top_parent = Foot

		if PS.create_sea_parent == True:
			# add sea empty:
			Sea = bpy.data.objects.new(name = PanoName + '_Sea', object_data = None)
			if PS.create_point_cols == False : # link to the current collection:
				bpy.context.collection.objects.link(Sea)
			top_parent.parent = Sea
			top_parent = Sea
			# add sea-related constraints
			# Sea parent :
			sea_p_constraint = Sea.constraints.new(type='SHRINKWRAP')
			sea_p_constraint.target = PS.sea_object
			sea_p_constraint.shrinkwrap_type = 'PROJECT'
			sea_p_constraint.project_axis = 'NEG_Z'
			sea_p_constraint.use_track_normal = True
			sea_p_constraint.track_axis = 'TRACK_Z'
			# Panorama
			pano_constraint = PanoSphere.constraints.new(type='COPY_ROTATION')
			pano_constraint.target = Sea
			# Foot
			foot_constraint = Foot.constraints.new(type='COPY_LOCATION')
			foot_constraint.target = Sea
			foot_constraint.use_z = False

		if PS.pano_location == 'specific':
			top_parent.location[0:2] = PanoLoc[0:2]

		altitude_parent.location[2] = PanoAltitude

		if PS.import_mode == 'batch' and PS.create_global_cols :
			self.CamsCol.objects.link(CamRender)
			self.PanosCol.objects.link(PanoSphere)
			if PS.create_ground_parent :
				self.GroundParentsCol.objects.link(Foot)
			if PS.create_sea_parent :
				self.SeaParentsCol.objects.link(Sea)
			
		if PS.create_point_cols :
			# Add collections :
			CamCol = bpy.data.collections.new(name=PanoName + '_Camera')
			PanoCol = bpy.data.collections.new(name=PanoName + '_Pano')
			bpy.context.scene.collection.children.link(CamCol)
			bpy.context.scene.collection.children.link(PanoCol)
			# Put the objects in it :
			CamCol.objects.link(CamRender)
			CamCol.objects.link(CamNavi)
			PanoCol.objects.link(PanoSphere)
			if PS.create_ground_parent :
				CamCol.objects.link(Foot)
			if PS.create_sea_parent :
				CamCol.objects.link(Sea)
		else:
			# link them to the current collection:
			bpy.context.collection.objects.link(CamRender)
			bpy.context.collection.objects.link(CamNavi)
			bpy.context.collection.objects.link(PanoSphere)
		
		# Sky ON:
		if PS.create_sky == True:
			pov_world=bpy.data.worlds.new(name='World_' + PanoName)
			pov_world.use_nodes=True
			tree=pov_world.node_tree
			pov_sky=tree.nodes.new(type='ShaderNodeTexSky')
			pov_sky.location=(-260,300)
			pov_sky.sun_elevation=SunElev
			pov_sky.sun_rotation=SunRot
			pov_sky.altitude=PanoAltitude
			pov_sky.air_density=PS.sky_air_density
			pov_sky.dust_density=PS.sky_dust_density
			pov_sky.ozone_density=PS.sky_ozone_density
			bgd = tree.nodes['Background']
			tree.links.new(pov_sky.outputs['Color'],bgd.inputs['Color'])
			bpy.context.scene.world=pov_world
		
		# Set render dimensions:
		width, height = bpy.data.images[ImgFilename].size
		context.scene.render.resolution_x = width
		context.scene.render.resolution_y = height
		
		# Set Camera projection:
		if PS.projection == "equi":
			CamRender.data.panorama_type = 'EQUIRECTANGULAR'
		elif PS.projection == "cyl":
			CamRender.data.panorama_type = 'CENTRAL_CYLINDRICAL'

class VIEW3D_OT_SetupManipulation(bpy.types.Operator):
	bl_idname = "setup.manipulation"
	bl_label = "Set up Manipulation"
	bl_description = "Prepare all the settings necessary for manipulation"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		C = bpy.context
		# Select 3D Cursor tool:
		bpy.ops.wm.tool_set_by_id(name = "builtin.cursor")
		# enable Surface Project and set Orientation to geometry:
		tool = ToolSelectPanelHelper.tool_active_from_context(context)
		props = tool.operator_properties('view3d.cursor3d')
		props.use_depth = True
		props.orientation = 'GEOM'
		# DEPRECATED SINCE 2.92: Disable Camera  Lock in preferences (to leave camera in place while walking):
		#bpy.context.preferences.inputs.use_camera_lock_parent = 0
		# set pivot point and transform orientation to 3D cursor
		bpy.ops.transform.select_orientation(orientation='CURSOR')
		C.scene.tool_settings.transform_pivot_point = 'CURSOR'
		# Info report
		self.report({'INFO'}, "Set up manipulation")
		return {'FINISHED'}

class VIEW3D_OT_RefreshImage(bpy.types.Operator):
	bl_idname = "image.refresh"
	bl_label = "Refresh image"
	bl_description = "Reload image file"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.context.object.material_slots[0].material.node_tree.nodes['Pano'].node_tree.nodes['Image Texture'].image.reload()
		# Info report
		self.report({'INFO'}, "Refreshed Image")
		return {'FINISHED'}

class VIEW3D_OT_CopyToClipboard(bpy.types.Operator):
	bl_idname = "copy.to_clipboard"
	bl_label = "Copy to clipboard"
	bl_description = ""
	bl_options = {'REGISTER', 'UNDO'}

	copied_value : bpy.props.FloatProperty(
		name = 'value',
		description = 'copied value',
		default = 2)

	def execute(self, context):
		rounded_value = round(self.copied_value,prefs().precision)
		bpy.context.window_manager.clipboard = str(rounded_value)
		# Info report
		self.report({'INFO'}, "Copied to clipboard")

		return {'FINISHED'}

class PanospheresPrefs(bpy.types.AddonPreferences):
	bl_idname = __name__

	category : StringProperty(
		description = 'Choose a name for the category of the panel',
		default = "Panospheres",
		update = update_Addon_category,
		)

	precision : IntProperty(
		description = 'Number of decimals in copied Y,P,R',
		default = 3
		)

	def draw(self, context):
		layout = self.layout
		box = layout.box()
		row = box.row(align=True)
		row.label(text="Viewport Panel Category:")
		row.prop(self, 'category', text="")
		row = box.row(align=True)
		row.label(text="Precision of copied Y,P,R values:")
		row.prop(self, 'precision', text="")

#class PanospheresProperties(bpy.types.PropertyGroup): {
PanospheresProperties = type(
    "PanospheresProperties",
    (bpy.types.PropertyGroup,), {
		"__annotations__": {

	# ----------------------------------------------------
	#				Global properties:
	# ----------------------------------------------------

			"import_mode": EnumProperty(
				name="Mode",
				description='Single import or batch import',
				items=[ ('single', "Single", ''),
						('batch', "Batch", ''),]
				),

			"projection": EnumProperty(
				name="Projection",
				description='Source image panoramic projection',
				items=[ ('equi', "Equirectangular", ''),
						('cyl', "Cylindrical", ''),]
				),
			
			"img_color_space" : EnumProperty(
				name="Image Color Space",
				description = 'Color space applied to the image',
				items=[ ('AgX Base sRGB', "AgX Base sRGB", ''),
						('Filmic sRGB', "Filmic sRGB", ''),
						('default', "default", ''),]
				),

			"create_ground_parent": BoolProperty(
				name="Ground parent",
				description='Adds a parent empty at Ground Altitude and moves the children up by given height',
				default = True,
				),

			"create_sea_parent": BoolProperty(
				name="Sea level parent",
				description='Adds a main parent empty at Z=0 and leaves children at Ground Altitude',
				default = False,
				),
			
			"sea_object": PointerProperty(
				name = "Sea",
				description = 'Sea object used for constraints',
				type = bpy.types.Object,
				),

			"create_global_cols": BoolProperty(
				name="Global collections",
				description='Create collections for various object types in the original scene',
				default = True,
				),

			"create_point_cols": BoolProperty(
				name="Per-point collections",
				description='Create separate collections for the camera rig and the panorama in each scene',
				default = True,
				),

			"create_scene": BoolProperty(
				name="New Scene",
				description='Create each Panosphere rig in a new linked scene',
				default = True,
				),

			"create_sky": BoolProperty(
				name="Sky",
				description='Creates a World with a Nishita Sky using provided values',
				default = True,
				),

			"pano_location": EnumProperty(
				name="X-Y Coord.",
				description='Location to import the panosphere to',
				items=[ ('zero', "World center", '', 'WORLD', 1),
						('specific', "3D Cursor", '', 'CURSOR', 2),
					]
				),

			"sky_air_density": FloatProperty(
				name = "Sky Air Density",
				default = 0.8,
				),

			"sky_dust_density": FloatProperty(
				name = "Sky Dust Density",
				default = 0,
				),

			"sky_ozone_density": FloatProperty(
				name = "Sky Ozone Density",
				default = 2.5,
				),

		# ----------------------------------------------------
		#				Single point properties:
		# ----------------------------------------------------
			"single_pano_name": StringProperty(
				name = "Name",
				description = "Name to give to the generated objects",
				),

			"single_image_path": StringProperty(
				name = "Image",
				description = "Path to the Panoramic image file",
				subtype = 'FILE_PATH',
				),

			"single_pano_HFOV": FloatProperty(
				name = "Image HFOV",
				description = 'Horizontal Field Of View',
				subtype = 'ANGLE',
				default = pi,
				),

			"single_pano_rotation": FloatProperty(
				name = "Pano rotation",
				description = "Panorama's Z Rotation (Counterclockwise)",
				subtype = 'ANGLE',
				default = 0,
				),

			"single_altitude": FloatProperty(
				name = "Ground Altitude",
				description = "Z height of the ground at the panorama's location",
				subtype = 'DISTANCE',
				default = 0,
				),

			"single_height_above": FloatProperty(
				name = "Height above ground",
				description = 'Camera height above empty',
				subtype = 'DISTANCE',
				default = 1.7,
				),

			"single_sun_elev": FloatProperty(
				name = "Sun elevation",
				description = 'in degrees',
				subtype = 'ANGLE',
				default = radians(20),
				),

			"single_sun_rot": FloatProperty(
				name = "Sun roation",
				description = 'Cartographic azimuth of the sun in degrees',
				subtype = 'ANGLE',
				default = pi,
				),

		# ----------------------------------------------------
		#				Batch mode-only properties:
		# ----------------------------------------------------
			"shp_collection": PointerProperty(
				name = "Collections",
				type = bpy.types.Collection,
				description = "Must be created as explained in the README",
				),

		},
	},
)

class IMPORT_PT_ui(bpy.types.Panel):
	bl_idname = "IMPORT_PT_main"
	bl_label = "Import"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Panospheres"

	def draw(self, context):
		layout = self.layout
		PS = context.scene.PS_props

		col = layout.column(align=True)
		col.prop(PS, 'import_mode')

		col = layout.column(align=True)
		col.prop(PS, 'projection')
		col = layout.column(align=True)
		split = col.split(factor = 0.5, align=True)
		col1 = split.row(align = True)
		col1.label(text='Image Color Space:')
		col2 = split.row(align = True)
		col2.prop(PS, 'img_color_space', text="")

		# Single mode:
		if (PS.import_mode == 'single'):

			col = layout.column()
			col.prop(PS, 'single_pano_name')

			col.prop(PS, 'single_image_path')
			row = layout.row(align = True)
			#col = row.column(align = True)

			split = row.split(factor = 0.23, align=True)
			left_col = split.column(align = True)
			right_col = split.column(align = True)
			right_col.prop(PS, 'single_pano_HFOV')
			right_col.prop(PS, 'single_pano_rotation')
			right_col.prop(PS, 'single_altitude')

			layout.separator(factor=2.0)
			col = layout.column()
			col.prop(PS, 'pano_location')

			col = layout.column(align = True)
			col.prop(PS, 'create_sky')
			if (PS.create_sky == True):
				row = layout.row(align = True)
				split = row.split(factor = 0.23)
				left_col = split.column(align = True)
				right_col = split.column(align = True)
				right_col.prop(PS, 'single_sun_elev')
				right_col.prop(PS, 'single_sun_rot')
				right_col.prop(PS, 'sky_air_density')
				right_col.prop(PS, 'sky_dust_density')
				right_col.prop(PS, 'sky_ozone_density')
			col = layout.column()
			col.prop(PS, 'create_ground_parent')
			if (PS.create_ground_parent == True):
				row = layout.row(align = True)
				split = row.split(factor = 0.23)
				left_col = split.column(align = True)
				right_col = split.column(align = True)
				right_col.prop(PS, 'single_height_above')
			col = layout.column()
			col.prop(PS, 'create_sea_parent')
			if (PS.create_sea_parent == True):
				col.prop(PS, 'sea_object')
			col = layout.column()
			col.prop(PS, 'create_point_cols', text = 'Collections')
			col.prop(PS, 'create_scene')

			layout.separator(factor=2.0)
			layout.operator("import.single", icon='IMPORT')
		# Batch mode:
		else:
			col = layout.column()
			col.prop(PS, 'create_sky', text = 'Skies')
			if (PS.create_sky == True):
				row = layout.row(align = True)
				split = row.split(factor = 0.23)
				left_col = split.column(align = True)
				right_col = split.column(align = True)
				right_col.prop(PS, 'sky_air_density')
				right_col.prop(PS, 'sky_dust_density')
				right_col.prop(PS, 'sky_ozone_density')
			col = layout.column()
			col.prop(PS, 'create_ground_parent', text = 'Ground parents')
			col.prop(PS, 'create_sea_parent', text = 'Sea level parents')
			if (PS.create_sea_parent == True):
				row = layout.row(align = True)
				split = row.split(factor = 0.23)
				left_col = split.column(align = True)
				right_col = split.column(align = True)
				right_col.prop(PS, 'sea_object')
			col = layout.column()
			col.prop(PS, 'create_global_cols')
			col.prop(PS, 'create_point_cols', text = 'Per-point collections')
			col.prop(PS, 'create_scene', text = 'New Scenes')

			col = layout.column(heading='Viewpoints Collection:')
			col.prop(PS, 'shp_collection', text='List')

			layout.separator(factor=2.0)
			layout.operator("import.batch", icon='IMPORT')

		return

class MANIPULATE_PT_ui(bpy.types.Panel):
	bl_idname = "MANIPULATE_PT_main"
	bl_label = "Manipulate"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Panospheres"

	def draw(self, context):

		try:
			layout = self.layout

			mat_params = context.object.material_slots[0].material.node_tree.nodes['Pano'].node_tree.nodes['Bright/Contrast']
			Lum = mat_params.inputs['Bright']
			Con = mat_params.inputs['Contrast']
			geo_modif = context.object.modifiers['Panosphere']

			layout.operator("setup.manipulation", icon='PIVOT_CURSOR')
			layout.operator('image.refresh', text = 'Refresh Image',icon = 'FILE_REFRESH')

			col = layout.column(align=True)
			col.prop(geo_modif, '["Input_9"]', text = 'HFOV')
			col.prop(geo_modif, '["Input_5"]', text = 'Conical Pitch')
			col.prop(Lum, 'default_value', text = Lum.name)
			col.prop(Con, 'default_value', text = Con.name)

			col = layout.column(align=True)
			col.label(text="Corrective results for Hugin:", icon = 'OUTLINER_OB_ARMATURE')

			split = col.split(factor = 0.85, align = True)
			split.prop(context.object, '["Yaw (optional)"]')
			copy = split.operator('copy.to_clipboard', text = '',icon = 'COPYDOWN')
			copy.copied_value = context.object['Yaw (optional)']

			split = col.split(factor=0.85, align=True)
			split.prop(context.object, '["Pitch"]')
			copy = split.operator('copy.to_clipboard', text = '',icon = 'COPYDOWN')
			copy.copied_value = context.object['Pitch']

			split = col.split(factor=0.85, align=True)
			split.prop(context.object, '["Roll"]')
			copy = split.operator('copy.to_clipboard', text = '',icon = 'COPYDOWN')
			copy.copied_value = context.object['Roll']


		except IndexError:
			row = layout.row(align=False)
			row.label(text="No panorama selected", icon="INFO")
			return

		except (KeyError, AttributeError) as e:
			row = layout.row(align=False)
			row.label(text="No imported panorama", icon="INFO")

		return

class OBJECT_OT_ImportSingle(bpy.types.Operator):
	bl_idname = "import.single"
	bl_label = "Import Single Panorama"
	bl_description = "imports selected image file and maps it to a Panosphere"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):

		PS = context.scene.PS_props

		sName = str(PS.single_pano_name)
		sPath = PS.single_image_path
		sHFOV = PS.single_pano_HFOV
		sRotation = -PS.single_pano_rotation
		sLoc = context.scene.cursor.location
		sAltitude = PS.single_altitude
		sHeight = PS.single_height_above
		sSunElev = PS.single_sun_elev
		sSunRot = PS.single_sun_rot

		import_panosphere(self, context, sName, sPath, sHFOV, sRotation, sLoc, sAltitude, sHeight, sSunElev, sSunRot)

		# Info report:
		self.report({'INFO'}, "Imported a panosphere")

		return {'FINISHED'}

class OBJECT_OT_ImportBatch(bpy.types.Operator):
	bl_idname = "import.batch"
	bl_label = "Import Multiple Panoramas"
	bl_description = "imports Panospheres using formatted collection"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):

		PS = context.scene.PS_props
		if PS.shp_collection:
			# memorize single mode pano Location type
			single_pano_location = PS.pano_location
			# force it to 'specific' (until end of operator) :
			PS.pano_location = 'specific'
			#First = True
			active_scene = bpy.context.scene
			
			if PS.create_global_cols :
				GlobalCol = bpy.data.collections.new(name='Panospheres Collections')
				self.CamsCol = bpy.data.collections.new(name='Panospheres Cameras')
				self.PanosCol = bpy.data.collections.new(name='Panospheres Panoramas')
				self.PanosCol.hide_viewport = True
				self.PanosCol.hide_render = True
				GlobalCol.children.link(self.CamsCol)
				GlobalCol.children.link(self.PanosCol)
				if PS.create_ground_parent :
					self.GroundParentsCol = bpy.data.collections.new(name='Panospheres Ground Parents')
					GlobalCol.children.link(self.GroundParentsCol)
				if PS.create_sea_parent :
					self.SeaParentsCol = bpy.data.collections.new(name='Panospheres Sea Parents')
					GlobalCol.children.link(self.SeaParentsCol)
			
			for point in PS.shp_collection.objects:

				bName = str(point['name'])
				bPath = point['image_path']
				bHFOV = radians(float(point['HFOV'].replace(',','.')))
				bRot = radians(float(point['rotation'].replace(',','.')))
				bLoc = point.location
				bAltitude = float(point['altitude'].replace(',','.'))
				bHeight = float(point['height'].replace(',','.'))
				bSunElev = radians(float(point['sun_elev'].replace(',','.')))
				bSunRot = radians(float(point['sun_rot'].replace(',','.')))

				import_panosphere(self, context, bName, bPath, bHFOV, bRot, bLoc, bAltitude, bHeight, bSunElev, bSunRot)

				# Remove the shapefile collection from each generated scene (if used, then also decomment "#First = True" line above):
				'''
				if PS.create_scene == True:
					if First :
						First = False
						bpy.context.scene.collection.children.unlink(PS.shp_collection)
				'''
				# The following ensures that the scene creation copies the original scene every time rather than copying each newly created scene:
				##### WARNING : can probably be deleted as well as the active_scene assignment above if scene creation code in import_panosphere is updated to the "full data way" instead of using operators :
				bpy.context.window.scene = active_scene

			# Link the global collection to the global scene
			if PS.create_global_cols :
				bpy.context.scene.collection.children.link(GlobalCol)

			# retrieve single mode pano Location type
			PS.pano_location = single_pano_location

			# Info report:
			self.report({'INFO'}, "Batch imported panospheres")
		else:
			ShowMessageBox(title="Please select a collection", icon='ERROR', text='')
		return {'FINISHED'}

# ------------------------------------------------------
# Registration
# ------------------------------------------------------
classes = (
	IMPORT_PT_ui,
	MANIPULATE_PT_ui,
	OBJECT_OT_ImportSingle,
	OBJECT_OT_ImportBatch,
	VIEW3D_OT_SetupManipulation,
	VIEW3D_OT_RefreshImage,
	VIEW3D_OT_CopyToClipboard,
	PanospheresPrefs,
	PanospheresProperties,
)

def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)

	bpy.types.Scene.PS_props = PointerProperty(type=PanospheresProperties)

	context = bpy.context
	prefs = context.preferences.addons[__name__].preferences
	update_Addon_category(prefs, context)

def unregister():
	del bpy.types.Scene.PS_props
	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls)

if __name__ == "__main__":
	register()
